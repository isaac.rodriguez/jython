package org.jython.book.interfaces;

public interface CostCalculatorType {

    public double calculateCost(double salePrice, double tax);

}