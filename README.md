# Jython

## **Introducción**

_Elegante es un adjetivo que se utiliza a menudo para describir el lenguaje Python. La palabra "elegante" se define como "agradablemente elegante y con estilo". Sin complicaciones y poderoso también podrían ser grandes palabras para ayudar en la descripción de este lenguaje. Es un hecho que Python es un lenguaje elegante que permite crear aplicaciones poderosas de una manera sencilla. La capacidad de hacer más fácil la lectura y escritura de software complejo es el objetivo de todos los lenguajes de programación, y Python hace precisamente eso._

_Aunque hemos definido fácilmente el objetivo de los lenguajes de programación en un sentido amplio en el párrafo uno, hemos omitido una de las principales ventajas de aprender el lenguaje de programación Python: Python se ha ampliado para que funcione en la plataforma Java, por lo que puede funcionar en cualquier lugar con una JVM. También hay versiones C y.NET de Python con soporte multiplataforma. Así que, Python puede correr casi por todas partes. Nos centramos en Jython, la implementación del lenguaje que toma la elegancia, potencia y facilidad de Python y la ejecuta en la JVM._

_La plataforma Java es un activo para el lenguaje Jython como lo son las bibliotecas C para Python. Jython es capaz de funcionar en casi todas partes, lo que le da mucha flexibilidad a la hora de decidir cómo implementar una aplicación. La plataforma Java no sólo permite flexibilidad en cuanto a la implementación de aplicaciones, sino que también ofrece una vasta biblioteca que contiene miles de APIs que están disponibles para su uso por parte de Jython. Añada la madurez de la plataforma Java y será fácil ver por qué Jython es un lenguaje de programación tan atractivo. El objetivo, si se quiere, de cualquier lenguaje de programación es conceder a sus desarrolladores la misma experiencia que Jython. En pocas palabras, aprender Jython será una ventaja para cualquier desarrollador._

_Como he mencionado, la implementación del lenguaje Jython toma Python y lo ejecuta en la JVM, pero hace mucho más que eso. Una vez que haya experimentado el poder de la programación en la plataforma Java, será difícil alejarse de ella. Aprender Jython no sólo le permite correr en la JVM, sino que también le permite aprender una nueva forma de aprovechar el poder de la plataforma. El lenguaje aumenta la productividad ya que tiene una sintaxis fácilmente comprensible que se lee casi como si fuera un pseudocódigo. También añade capacidades dinámicas que no están disponibles en el propio lenguaje Java._


## **Diferencia entre Python y Jython**

_Jython es una implementación del lenguaje Python para la plataforma Java. Hay tres implementaciones principales de Python. Estas implementaciones son: CPython, Jython para la plataforma Java e IronPython para la plataforma.NET. En el momento de escribir este artículo, CPython es la más frecuente de las implementaciones. Por lo tanto, si usted ve la palabra Python en alguna parte, bien podría estar refiriéndose a esa implementación._

### Contenido

* [**Chapter 8 Modulos**](https://gitlab.com/isaac.rodriguez/jython/tree/master/chapter08)

    - Los módulos pueden ser usados para separar lógicamente el código que pertenece a un mismo código, haciendo que los programas sean más fáciles de entender. Los módulos son útiles para crear bibliotecas que se pueden importar y utilizar en diferentes aplicaciones que comparten alguna funcionalidad. La biblioteca estándar de Jython viene con un gran número de módulos que se pueden utilizar en sus programas de inmediato.

* [**Chapter 9 Scripts**](https://gitlab.com/isaac.rodriguez/jython/tree/master/chapter09)

    - Los scripts con Jython. Para nuestros propósitos, definiremos 'scripting' como la escritura de pequeños programas para ayudar en las tareas diarias. Estas tareas son cosas como borrar y crear directorios, administrar archivos y programas, y cualquier otra cosa que se sienta repetitiva y que pueda expresar como un programa pequeño. En la práctica, sin embargo, los guiones pueden llegar a ser tan grandes que la línea entre un guión y un programa de tamaño completo puede hacerse borrosa.

* [**Chapter 10 Integración Java y Python**](https://gitlab.com/isaac.rodriguez/jython/tree/master/chapter10)

    - La integración Java es el corazón del desarrollo de aplicaciones de Jython. La mayoría de los desarrolladores de Jython son desarrolladores de Python que buscan hacer uso de la vasta biblioteca de herramientas que la JVM tiene para ofrecer, o desarrolladores de Java que desean utilizar la semántica del lenguaje Python sin migrar a una plataforma completamente diferente. El hecho es que la mayoría de los desarrolladores de Jython lo utilizan para poder aprovechar las vastas librerías disponibles en el mundo Java, y para ello es necesaria una cierta integración de Java en la aplicación. Ya sea que planee utilizar algunas de las librerías Java existentes en su aplicación, o que esté interesado en mezclar un poco de código Python en su aplicación Java.

* [**Chapter 11 Uso de Jython en un IDE**](https://gitlab.com/isaac.rodriguez/jython/tree/master/chapter11)

    - Eclipse y Netbeans. Hay muchos otros entornos de desarrollo disponibles para Python y Jython hoy en día; sin embargo, estos dos son quizás los más populares y contienen la mayoría de las herramientas específicas de Jython. Eclipse ha tenido un plug-in conocido como PyDev durante varios años, y este plug-in proporciona un amplio soporte para el desarrollo y mantenimiento de aplicaciones Python y Jython por igual. Netbeans comenzó a incluir soporte para Python y Jython con la versión 6.5 y posteriores. El IDE Netbeans también proporciona un amplio soporte para el desarrollo y mantenimiento de aplicaciones Python y Jython.